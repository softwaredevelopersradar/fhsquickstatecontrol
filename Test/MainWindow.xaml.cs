﻿using System.Windows;
using System;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Fps.numOfLetters =10;
            SetResourcelanguage();
        }

        private void ButtonCheck_Click(object sender, RoutedEventArgs e)
        {
            byte[] mass = new byte[10] { 1, 2, 3, 4 , 250,238,102, 77, 36, 0};
            Fps.UpdateStates(mass);
        }

        private void ButtonCheck1_Click(object sender, RoutedEventArgs e)
        {
            byte[] mass = new byte[5] { 0, 5, 80, 3 , 228};

            Fps.UpdateStates(mass);
        }

        private void ButtonPow_Click(object sender, RoutedEventArgs e)
        {
            var codes = new short[6] { 11, 11, 1, 11, 1, 1 };
            Fps.UpdateHalfPow(codes);
        }

        private void SetResourcelanguage()
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                //switch (Language)
                //{
                //    case Languages.Eng:
                //dict.Source = new Uri("/FpsStateControl;component/Languages/StringResource.EN.xaml",
                //              UriKind.Relative);
                //        break;
                //    case Languages.Rus:
                dict.Source = new Uri("/FpsStateControl;component/Languages/StringResource.xaml",
                                   UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/FpsStateControl;component/Languages/StringResource.xaml",
                //                          UriKind.Relative);
                //        break;
                //}

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
    }
}
