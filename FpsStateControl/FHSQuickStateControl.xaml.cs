﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace FpsStateControl
{
    /// <summary>
    /// Логика взаимодействия для FHSQuickStateControl.xaml
    /// </summary>
    public partial class FHSQuickStateControl : UserControl
    {
        List<byte> listPow = new List<byte>{0,100,95,90,85,80,75,70,65,60,55,50,45,40,35,30,25,20,15,10}; //все проценты мощности
        public event EventHandler<SetHalfPower> OnSetPower = (object sender, SetHalfPower data) => { }; //событие для сервера
        public event EventHandler<SetLoad> OnSetLoad = (object sender, SetLoad data) => { }; //событие для сервера
        public event EventHandler OnUpdateAll;
        public int numOfLetters { get; set; } =9;

        protected virtual void OnWithoutParam(EventHandler some_ev) => some_ev?.Invoke(this, null);

        public FHSQuickStateControl()
        {
            InitializeComponent();
            paramsFps.DataContext = new GlobalState();
            paramsFps.ItemsSource = (paramsFps.DataContext as GlobalState).CollectionStates;
            AddEmptyRows(0);
        }


        private void DataGridParamsFps_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(paramsFps.Items.Count);
        }

        /// <summary>
        /// Обновить данные таблицы
        /// </summary>
        public void UpdateStates(byte[] mass)
        {
            try
            {
                ((GlobalState)paramsFps.DataContext).TempCollection = new ObservableCollection<TableStateParams>(((GlobalState)paramsFps.DataContext).CollectionStates);
                ((GlobalState)paramsFps.DataContext).CollectionStates.Clear();

                for (int i = 0; i < mass.Length; i++)
                {
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Add(new TableStateParams(mass[i]) { letter = ((GlobalState)paramsFps.DataContext).TempCollection[i].letter, settedPow = ((GlobalState)paramsFps.DataContext).TempCollection[i].settedPow }); 

                 }
                //if (mass.Length == 10)
                //{
                //    ((GlobalState)paramsFps.DataContext).CollectionStates[9].load = 1;
                //}
                paramsFps.ItemsSource = ((GlobalState)paramsFps.DataContext).CollectionStates;
                AddEmptyRows(mass.Length);
            }
            catch { }
        }

        /// <summary>
        /// Удалить литеры, где нет связи с УМ
        /// </summary>
        public void DeleteNullStates(List<int> litersNull)
        {
            try
            {
                ((GlobalState)paramsFps.DataContext).TempCollection = new ObservableCollection<TableStateParams>(((GlobalState)paramsFps.DataContext).CollectionStates);
                for (int i = 0; i < litersNull.Count; i++)
                { 
                    ((GlobalState)paramsFps.DataContext).CollectionStates.RemoveAt(litersNull[i]);
                    ((GlobalState)paramsFps.DataContext).TempCollection[litersNull[i]] = new TableStateParams() { letter = ((GlobalState)paramsFps.DataContext).TempCollection[litersNull[i]].letter };
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Insert(litersNull[i], ((GlobalState)paramsFps.DataContext).TempCollection[litersNull[i]]);
                }
                paramsFps.ItemsSource = ((GlobalState)paramsFps.DataContext).CollectionStates;
            }
            catch { }
        }

        /// <summary>
        /// (Временно для Володи) Пишет номер литеры = 10
        /// </summary>
        public void UpdateStatesFake(byte[] mass)
        {
            try
            {
                ((GlobalState)paramsFps.DataContext).TempCollection = new ObservableCollection<TableStateParams>(((GlobalState)paramsFps.DataContext).CollectionStates);
                ((GlobalState)paramsFps.DataContext).CollectionStates.Clear();

                for (int i = 0; i < mass.Length; i++)
                {
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Add(new TableStateParams(mass[i]) { letter = 10, settedPow = ((GlobalState)paramsFps.DataContext).TempCollection[i].settedPow });
                }
                paramsFps.ItemsSource = ((GlobalState)paramsFps.DataContext).CollectionStates;
                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// (Временно для Володи) очищение всей таблицы
        /// </summary>
        public void ClearTable()
        {
            try
            {
                ((GlobalState)paramsFps.DataContext).CollectionStates.Clear();
                
                paramsFps.ItemsSource = ((GlobalState)paramsFps.DataContext).CollectionStates;
                AddEmptyRows();
            }
            catch { }
        }

        /// <summary>
        /// Обновить столбец %POW
        /// </summary>
        public void UpdateHalfPow(short[] powerCode)
        {
            try
            {
                ((GlobalState)paramsFps.DataContext).TempCollection = new ObservableCollection<TableStateParams>(((GlobalState)paramsFps.DataContext).CollectionStates);
                for (int i = 0; i < powerCode.Length; i++)
                {
                    ((GlobalState)paramsFps.DataContext).CollectionStates.RemoveAt(i);
                    ((GlobalState)paramsFps.DataContext).TempCollection[i].settedPow = listPow[powerCode[i]];
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Insert(i, ((GlobalState)paramsFps.DataContext).TempCollection[i] );
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = paramsFps.Items.Count; // количество всех строк в таблице
                int maxRows = numOfLetters;
                for (int i = 0; i < maxRows - сountRowsAll; i++)
                {
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Add(new TableStateParams() { letter = i+1 });
                }
            }
            catch { }
        }

        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows(int lastOne)
        {
            try
            {
                int сountRowsAll = paramsFps.Items.Count; // количество всех строк в таблице
                int maxRows = numOfLetters;
                for (int i = lastOne; i < maxRows; i++)
                {
                    ((GlobalState)paramsFps.DataContext).CollectionStates.Add(new TableStateParams() { letter = i + 1 });
                }
            }
            catch { }
        }

        /// <summary>
        /// Установить процент мощности
        /// </summary>
        private void BtnSetPow_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                var litNum = Convert.ToByte(Convert.ToInt32(ListLetter.Text));
                var halfPow = Convert.ToByte(listPow.IndexOf(Convert.ToByte(Convert.ToInt32(ListPow.Text))));
                var setPow =  new SetHalfPower { Letter = litNum, Power = halfPow };
                OnSetPower(sender, setPow);
            }
            catch { }
        }

        /// <summary>
        /// Установить тип нагрузки
        /// </summary>
        private void BtnLoadPow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte typeLoad;
                var litNum = Convert.ToByte(Convert.ToInt32(ListLetter.Text));
                if (ListLoad.Text == "EQV")
                {
                    typeLoad = 0;
                }
                else typeLoad = 1;
                var setLoad = new SetLoad { Letter = litNum, Load = typeLoad };
                OnSetLoad(sender, setLoad);
            }
            catch { }
            
        }

        private void BtnUpdateAll_Click(object sender, RoutedEventArgs e)
        {
            OnWithoutParam(OnUpdateAll);
        }

        
    }


}
