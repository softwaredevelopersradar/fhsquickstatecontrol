﻿namespace FpsStateControl
{
    internal class TableStateParams 
    {
        public int letter { get; set; } = 0;
        public int temp { get; set; } = 2;
        public int ucr { get; set; } = 2;
        public int icr { get; set; } = 2;
        public int pcr { get; set; } = 2;
        public int cvs { get; set; } = 2;
        public int evs { get; set; } = 2;
        public int pow { get; set; } = 2;
        public int load { get; set; } = 2;
        public short settedPow { get; set; } = 0;

        internal TableStateParams()
        {
        }

        internal TableStateParams(byte state)
        {
            temp = state & 1;
            ucr = (state >> 1) & 1;
            icr = (state >> 2) & 1;
            pcr = (state >> 3) & 1;
            cvs = (state >> 4) & 1;
            evs = (state >> 5) & 1;
            //pow = (state >> 6) & 1;
            load = (state >> 6) & 1;
        }
    }
}
