﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FpsStateControl
{
    public class GlobalState : INotifyPropertyChanged
    {
        internal ObservableCollection<TableStateParams> CollectionStates { get; set; }
        internal ObservableCollection<TableStateParams> TempCollection { get; set; }

        public GlobalState()
        {
           CollectionStates = new ObservableCollection<TableStateParams> { };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
