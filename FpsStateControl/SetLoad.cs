﻿
namespace FpsStateControl
{
    public struct SetLoad
    {
        public byte Letter { get; set; }
        public byte Load { get; set; }

        public SetLoad(byte letter, byte power)
        {
            Letter = letter;
            Load = power;
        }
        public SetLoad(byte[] bytes, int startIndex = 0)
        {
            try
            {
                Letter = bytes[startIndex];
                Load = bytes[startIndex + 1];
            }
            catch
            {
                Letter = 0;
                Load = 0;
            }
        }

    }
}
