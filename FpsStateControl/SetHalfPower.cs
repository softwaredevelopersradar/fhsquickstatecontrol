﻿
namespace FpsStateControl
{
    public struct SetHalfPower
    {
        public byte Letter { get; set; }
        public byte Power { get; set; }

        public SetHalfPower(byte letter, byte power)
        {
            Letter = letter;
            Power = power;
        }
        public SetHalfPower(byte[] bytes, int startIndex = 0)
        {
            try
            {
                Letter = bytes[startIndex];
                Power = bytes[startIndex + 1];
            }
            catch
            {
                Letter = 0;
                Power = 0;
            }
        }

    }
}
